import React, {Component} from 'react';
import {AiFillMessage} from "react-icons/ai";
class Icon extends Component {
    render() {
        return (
            <>
                 <span className="icon-message">
                                                <span className="text-white font-size-15"><AiFillMessage/></span>
                                            </span>
            </>
        );
    }
}

export default Icon;
