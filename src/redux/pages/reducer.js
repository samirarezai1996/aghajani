import {PAGE_NUMBER, PAGE_NUMBER_DRIVER} from 'redux/pages/type';

export function pageNumber(state = {terminal: '1',driver: '1'}, action) {
    switch (action.type) {
        case PAGE_NUMBER:
            return {...state,terminal: action.i};
        case PAGE_NUMBER_DRIVER:
            return {...state,driver: action.i};
        default:
            return state;
    }
}
