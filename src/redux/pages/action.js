import {PAGE_NUMBER,PAGE_NUMBER_DRIVER} from 'redux/pages/type';
export function pageNum(i,name) {
    switch (name) {
        case 'driver':
            return ({
                type: PAGE_NUMBER_DRIVER,
                i
            });
        case 'terminal':
            return ({
                type: PAGE_NUMBER,
                i
            });
    }

}
