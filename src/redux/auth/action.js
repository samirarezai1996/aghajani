import {GET_OTP, CONFIRM_OTP} from 'redux/auth/type';
import {fetcher, reset} from 'redux/common/action';
import {removeToken} from "utils/utils";
import {message} from "antd";

export const resultGetOtp = (data, number) => {
    return {
        type: GET_OTP,
        data: {
            data,
            number
        }
    }
};
export const resultConfirmOtp = (data) => {
    return {
        type: CONFIRM_OTP,
        data
    }
};

export const logOut = () => {
    removeToken();
    return (dispatch) => {
        dispatch(resultConfirmOtp({
            data: {
                result: false
            }
        }));
        dispatch(reset());
        window.location = '/#/'

    }
};

export function getOtp(data) {
    return (dispatch) => {
        return fetcher('/account/auth/send_otp/',
            {
                method: 'POST',
                body: JSON.stringify(data),
            }, {noAuth:false})
            .then((response) => {
                /*message.success(response?.result || 'code')*/
            }).catch((e) => {
                message.error(e.message)
            })
    };
}

export function confirmOtp(data) {
    return (dispatch) => {
        return fetcher('/account/auth/verify_otp/',
            {
                method: 'POST',
                body: JSON.stringify(data),
            })
            .then((response) => {
                if(response?.result?.message){
                    message.error(response?.result?.message);
                }else {
                    dispatch(resultConfirmOtp(response));
                }
            })
            .catch((e) => {
                if (e.message === '406')
                    message.error('کد صحیح را وارد نمایید');
                else
                    message.error(e.message)
            })
    }
}

export function refreshOtp(data) {
    return (dispatch) => {
        return fetcher('/account/auth/token/refresh',
            {
                method: 'POST',
                body: JSON.stringify(data),
            }, {noAuth:false})
            .then((response) => {
                dispatch(resultConfirmOtp(response));
            })
    };
}
